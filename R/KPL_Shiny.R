#' @title 'Koll på läget' (KPL) som shiny-app
#' @description Denna funktion används för att skapa en lokal/INCA-baserad "Koll på Läget" (KPL) som shiny-app.
#'
#' @param inca ska output genereras som om på INCA-plattformen?
#' @param incaScript script som ska göras efter inladdning av data på INCA-plattformen. Ska generera en data frame 'df' som skickas in till rccKPL::KPL().
#' @param data data frame som skickas vidare som 'df' i anropet till rccKPL::KPL() om ej på INCA.
#' @param ... ytterligare parameterar som skickas in till rccKPL::KPL()
#'
#' @details ...
#'
#' @return En shiny-app innehållande html-version av Koll på läget.
#'
#' @examples # För testning lokalt:
#' KPL_Shiny(
#'   inca = FALSE,
#'   data = exampledata_KPL,
#'   år = "2010",
#'   huvud_text = "[huvud_text]",
#'   sub_text_1 = "[sub_text_1]",
#'   sub_text_2 = "[sub_text_2]",
#'   sub_text_3 = "[sub_text_3]",
#'   block_1 = "[block_1]",
#'   block_1_text = "[block_1_text]",
#'   block_2 = "[block_2]",
#'   block_2_text = "[block_2_text]",
#'   block_3 = "[block_3]",
#'   block_3_text = "[block_3_text]",
#'   enhets_nivå_1 = "[enhets_nivå_1]",
#'   enhets_nivå_2 = "[enhets_nivå_2]",
#'   enhets_nivå_3 = "[enhets_nivå_3]",
#'   indikator_text = "[indikator_text]",
#'   antal_text = "[antal_text]",
#'   måluppfyllelse_text = "[måluppfyllelse_text]",
#'   beskrivning_block_1_titel = "[beskrivning_block_1_titel]",
#'   beskrivning_block_1_text = "[beskrivning_block_1_text]",
#'   beskrivning_block_2_titel = "[beskrivning_block_2_titel]",
#'   beskrivning_block_2_text = "[beskrivning_block_2_text]",
#'   beskrivning_block_3_titel = "[beskrivning_block_3_titel]",
#'   beskrivning_block_3_text = "[beskrivning_block_3_text]",
#'   beskrivning_block_4_titel = "[beskrivning_block_4_titel]",
#'   beskrivning_block_4_text = "[beskrivning_block_4_text]"
#' )
#' @export

KPL_Shiny <- function(
    inca = FALSE,
    incaScript = NULL,
    data = NULL,
    ...
){
  ui <- fluidPage(
    div(
      id = "div_loading_page",
      HTML('<div class="alert alert-info" style = "color: #31708f !important; background-color: #d9edf7 !important; border-color: #bce8f1 !important;font-family: Arial, Helvetica, sans-serif"><i class="fa fa-spinner fa-spin"></i> Var god vänta medan rapporten förbereds...</div>')
    ),
    shinyjs::hidden(
      div(
        id = "div_main_page",
        shinyjs::useShinyjs(),
        tags$head(
          tags$style(HTML(".tooltip {opacity: 1;}")), # Annars orsakar class tooltip att titeln blir osynlig
          tags$script(
            src = "https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.16/iframeResizer.contentWindow.min.js",
            type = "text/javascript"
          )
        ),
        htmlOutput(outputId = "kpl"),
        HTML("<div data-iframe-height></div>")
      )
    )
  )

  server <- function(input, output, session) {
    if (inca) {
      # INCA: Ladda data och kör script som ska generera 'df'
      if (requireNamespace("INCA", quietly = TRUE)) {
        incaEnv <- INCA::getDataFrames(shinySession = session)
        for (i in ls(envir = incaEnv)) {
          assign(x = i, value = get(i, envir = incaEnv))
        }
      } else {
        warning(
          "inca = TRUE men R-paketet 'INCA' finns inte installerat (körningen görs troligtvis ej på INCA). OK för testning."
        )
      }
      source(incaScript, encoding = "UTF-8", local = TRUE)
      if (!exists("df")) {
        stop("Scriptet '", incaScript, "' måste generera en data frame 'df'", call. = FALSE)
      }
      if (!is.data.frame(df)) {
        stop("Scriptet '", incaScript, "' måste generera en data frame 'df'", call. = FALSE)
      }
    } else {
      # Lokalt: Kräv 'data'
      if (is.null(data))
        stop("'data' måste anges om inca = FALSE", call. = FALSE)
      df <- data
    }

    output$kpl <- renderUI({
      HTML(
        KPL(
          df = df,
          return_html = TRUE,
          ...
        )
      )
    })

    # Döljer laddningsskärmen och visar rapporten när allt är klart
    observeEvent(TRUE, once = TRUE, {
      shinyjs::delay(1500, shinyjs::hide("div_loading_page"))
      shinyjs::delay(1500, shinyjs::show("div_main_page", anim = TRUE, animType = "fade", time = 1))
    })
  }

  shinyApp(ui = ui, server = server)
}
