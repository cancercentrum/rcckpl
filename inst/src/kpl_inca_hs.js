var colors = ["#00B3F6", "#FFB117", '#F8F8F8', "#80B286"];
var colors_tint = ['#80A3C3', '#CCCCCC'];

var defaultChart1 = {
    chartContent: null,
    highchart: null,
    defaults: {
        credits: {
            enabled: false
        },
        chart: {
            type: 'bar',
            plotBackgroundColor: colors[2],
            margin: [0, 20, 12, 0],
            plotBorderWidth: 0,
            plotBorderColor: colors[2]
        },
        plotOptions: {
            bar: {
                pointWidth: 15
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['', ''],
            labels: {
                style: {
                    fontSize: '12px',
                    fontFamily: 'verdana',
                    width: '35px',
                    color: '#000000'
                }
            },
            lineColor: 'transparent',
            tickLength: 0
        },
        legend: {
            enabled: false
        }
    },
    init: function (options) {
        this.highchart = jQuery.extend({}, this.defaults, options);
        this.highchart.chart.renderTo = this.chartContent;
    },
    create: function () {
        return new Highcharts.Chart(this.highchart);
    }
};

var defaultChart2 = {
    chartContent: null,
    highchart: null,
    defaults: {
        credits: {
            enabled: false
        },
        chart: {
            type: 'column',
            plotBackgroundColor: colors[2],
            margin: [0, 20, 12, 0],
            plotBorderWidth: 0,
            plotBorderColor: colors[2]
        },
        plotOptions: {
            column: {
                pointWidth: 25
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            min: 0,
            max: 100,
            lineColor: 'transparent',
            tickLength: 0,
            title: {
                text: ''
            },
            labels: {
                enabled: false
            },
            gridLineWidth: 0
        },
        tooltip: {
            enabled: false
        },
        legend: {
            enabled: false
        }
    },
    init: function (options) {
        this.highchart = jQuery.extend({}, this.defaults, options);
        this.highchart.chart.renderTo = this.chartContent;
    },
    create: function () {
        return new Highcharts.Chart(this.highchart);
    }
};

function createChart1(containerName, y1, y2, y3, l1, l2, stackednames) {

    var series = null, tempTooltip = false;
    if (y1 !== null && y1.length === 2) {
        tempTooltip = true;
        series = [
            {
                name: stackednames[1],
                data: [{y: y1[1], color: colors_tint[0], borderColor: colors_tint[0]}, {
                    y: y2[1],
                    color: colors_tint[1],
                    borderColor: colors_tint[1]
                }]
            },
            {
                name: stackednames[0],
                data: [{y: y1[0], color: colors[0], borderColor: colors[0]}, {
                    y: y2[0],
                    color: colors[1],
                    borderColor: colors[1]
                },]
            }
        ];
    } else {
        series = [
            {
                data: [{y: y1, color: colors[0], borderColor: colors[0]}, {
                    y: y2,
                    color: colors[1],
                    borderColor: colors[1]
                }, {y: y3, color: colors[3], borderColor: colors[3]}]
            }
        ];
    }

    var tempTickPos = [0, l1, l2, 100];

    var tempChart = {
        chartContent: containerName,
        options: {
            yAxis: {
                labels: {
                    formatter: function () {
                        if (this.value === l1 || this.value === l2) {
                            return this.value;
                        } else {
                            return '';
                        }
                    },
                    style: {
                        fontSize: '8px',
                        fontFamily: 'verdana',
                        color: '#606060'
                    },
                    y: 12
                },
                tickPositions: tempTickPos,
                gridLineWidth: 0,
                plotLines: [{
                    color: '#606060',
                    width: 1,
                    value: l1,
                    zIndex: 5
                }, {
                    color: '#606060',
                    width: 1,
                    value: l2,
                    zIndex: 5
                }]
            },
            plotOptions: {
                bar: {
                    pointWidth: 15
                },
                series: {
                    stacking: 'normal'
                }
            },
            tooltip: {
                enabled: tempTooltip,
                formatter: function () {
                    return '<b>' + this.series.name + '</b>';
                }
            },
            series: series
        }
    };
    tempChart = jQuery.extend(true, {}, defaultChart1, tempChart);
    tempChart.init(tempChart.options);
    var chartFinal = tempChart.create();

    chartFinal.renderer.text('%', 130, 80).css({
        fontSize: '8px',
        fontFamily: 'verdana',
        color: '#606060',
        textAlign: 'left'
    }).attr({
        zIndex: 999
    }).add();

    return chartFinal;
}

function createChart2(containerName, y) {

    var ytxt = y.slice(0);
    for (var i = 0; i < ytxt.length; i++) {
        if (!ytxt[i] && ytxt[i] !== 0) {
            ytxt[i] = '-';
        }
    }

    var tempChart = {
        chartContent: containerName,
        options: {
            xAxis: {
                categories: ytxt,
                tickLength: 0,
                labels: {
                    style: {
                        fontSize: '8px',
                        fontFamily: 'verdana',
                        color: '#606060'
                    },
                    y: 12
                }
            },
            series: [
                {
                    data: [{y: y[0], color: colors[0], borderColor: colors[0]}, {
                        y: y[1],
                        color: colors[0],
                        borderColor: colors[0]
                    }, {y: y[2], color: colors[0], borderColor: colors[0]}, {
                        y: y[3],
                        color: colors[0],
                        borderColor: colors[0]
                    }]
                }
            ]

        }
    };
    tempChart = jQuery.extend(true, {}, defaultChart2, tempChart);
    tempChart.init(tempChart.options);
    var chartFinal = tempChart.create();

    chartFinal.renderer.text('%', 130, 80).css({
        fontSize: '8px',
        fontFamily: 'verdana',
        color: '#606060',
        textAlign: 'left'
    }).attr({
        zIndex: 999
    }).add();

    return chartFinal;
}

function createIndRow(counter, shorttitle, longtitle, num1, den1, num2, den2, num3, den3, placnum, placden, history, l1, l2, group, description, stackednames, groupdividers, grouptxt1, grouptxt2, tooltip) {

    var stacked = false;

    var pct1 = [null, null],
        pct2 = [null, null],
        pct3 = [null, null],
        sign = null,
        sumnum1 = null,
        sumpct1 = null,
        sumpct2 = null,
        sumpct3 = null;

    if (num1 && num1.length === 2) {
        stacked = true;
    }

    if (stacked === true) {
        if (den1 && den1 > 0) {
            for (var i = 0; i < 2; i++) {
                pct1[i] = 100 * num1[i] / den1;
                sumnum1 = sumnum1 + num1[i];
                sumpct1 = sumpct1 + pct1[i];
            }
            sign = 3;
            if (sumpct1 >= l1) {
                sign = 2;
            }
            if (sumpct1 >= l2) {
                sign = 1;
            }
            sumpct1 = Math.round(sumpct1);
        }
        if (den2 && den2 > 0) {
            for (i = 0; i < 2; i++) {
                pct2[i] = 100 * num2[i] / den2;
                sumpct2 = sumpct2 + pct2[i];
            }
            sumpct2 = Math.round(sumpct2);
        }
        if (den3 && den3 > 0) {
            for (i = 0; i < 2; i++) {
                pct3[i] = 100 * num3[i] / den3;
                sumpct3 = sumpct3 + pct3[i];
            }
            sumpct3 = Math.round(sumpct3);
        }
        for (var i = 0; i < history.length; i++) {
            if (history[i]) {
                if(history[i] < 0) {
                    history[i] = 0;
                }
                history[i] = Math.round(history[i]);
            }
        }
    } else {
        pct1 = null,
            pct2 = null,
            pct3 = null,
            sign = null,
            sumnum1 = null,
            sumpct1 = null,
            sumpct2 = null,
            sumpct3 = null;

        if (den1 && den1 > 0) {
            pct1 = 100 * num1 / den1;
            sign = 3;
            if (pct1 >= l1) {
                sign = 2;
            }
            if (pct1 >= l2) {
                sign = 1;
            }
            sumnum1 = num1;
            sumpct1 = Math.round(pct1);
        }
        if (den2 && den2 > 0) {
            pct2 = 100 * num2 / den2;
            sumpct2 = Math.round(100 * num2 / den2);
        }
        if (den3 && den3 > 0) {
            pct3 = 100 * num3 / den3;
            sumpct3 = Math.round(100 * num3 / den3);
        }
        for (i = 0; i < history.length; i++) {
            if (history[i]) {
                if(history[i] < 0) {
                    history[i] = 0;
                }
                history[i] = Math.round(history[i]);
            }
        }
    }

    var returnpoint = 0;

    var table = document.getElementById('bodyTable'),
        tr = document.createElement('tr'),
        td,
        content;

    var indexGroup1 = $.inArray(counter, groupdividers);
    if (indexGroup1 > -1) {
		if (grouptxt1[indexGroup1] != '') {
			tr_title = document.createElement('tr');
			td_margin = document.createElement('td');
			$(td_margin).attr('class', 'margin');
			tr_title.appendChild(td_margin);
			td_title = document.createElement('td');
			$(td_title).attr('colspan', '6');
			content = document.createElement('div');
			$(content).attr('style', 'font-size:14px;font-weight:bold;margin:4px 0px 4px 0px;');
			$(content).html(grouptxt1[indexGroup1]);
			td_title.appendChild(content);
			content = document.createElement('div');
			$(content).attr('style', 'font-size:12px;margin:0px 0px 4px 0px;');
			$(content).html(grouptxt2[indexGroup1]);
			td_title.appendChild(content);
			tr_title.appendChild(td_title);	
			td_margin = document.createElement('td');
			$(td_margin).attr('class', 'margin');
			tr_title.appendChild(td_margin);	
			table.appendChild(tr_title);
		}
    }
	var indexGroup2 = $.inArray(counter+1, groupdividers);
    if (indexGroup2 > -1) {
        $(tr).attr('class', 'divider');
    }

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsign');
    if (sign !== null) {

        returnpoint = 3 - sign;

        content = document.createElement('img');
        $(content).attr('width', '35px');

        if (sign == 1) {
            $(content).attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAApVBMVEUAAACXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iXz0iv2XnM5q3g8M7m89jZ7cPF46H////z+eyf0lq23If5/Pan1mrS6bjt9uK+4JUAAADXzLK5AAAAJnRSTlMAA0KFud/wQxaH5wZ89X0mzyc/7/YH0HsXhgToRYm44PH3KNHpiMoO7BAAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAABM0lEQVQ4y5WUfV+CMBCADxB0CIYKRliKZgchpKV9/6/WAIvbC1TPfzee33Z3bAfQYZjWyHbQsUfW2AAdE+Zih8smijH1fBTxZ1NRuQtQJZhTZbFEHctFp5g+6gnNb2XuYB/O7bgowH5WUePc4xBercQkmSx/LYpjKaQUc+ehi/Oq5S0jEgNIXNWpCuKsEzBJmBflCc/vXPogq49gkaj9cuHOmaw+wUappD6RJrSBreJcedI0tiHVbZPThRRkJZPK4sj7ZLyq6wnFfcR8PlWF5yPUVfKDjpLC66L9uTQ9bhD6Q/tcVD8IfU7Wvzj8fwHDYVh9f8JBpbk/4A06XnufVwPK7T7/5V3w97XrUXbmv94pwP5Zl8teHArRTG5BOIuU6RKzAzEOL7F2ShljPsdSTOU59gW2HY9q/iYk8gAAAABJRU5ErkJggg==');
        } else if (sign == 2) {
            $(content).attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAjVBMVEUAAAD5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL5sDL816b6vFz6wW3////7x33958r94b8AAADJ+PAbAAAAJnRSTlMAA0KFud/wQxaH5wZ89X0mzyc/7/YH0HsXhgToRYm44PH3KNHpiMoO7BAAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAABB0lEQVQ4y5WU7XaCMAxAQxG0CgwVHMOPoXMpK/P9X2/VuUNDS8vuv/bc06ZpEoCegIWzKMY4moXzAGws+BJ7lnxhGKskRUqarajykqNJvtaVzRZtbDe9wlK0U7A/ZR3jGPHzujLHcXblw3lFF8ldqWgwQoiWhFQp540oX1LKjuxwgFrLbiu+peHsa2DaspPSdPAAodc5woludKZzgnevE0HjdRpAr4OTzpkSz5R36flphbgp50a/7GjL8+CoA9R7j6P+Czi64ff6KZzKo34gcTrJbz3vHMqznqf0heqv84hyZv/qU4DLhy2WCx0KZTZMQZGVxnSp+FUzrp+VdUoFczXHGmyGc+wHIdiOCuotaU4AAAAASUVORK5CYII=');
        } else if (sign == 3) {
            $(content).attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAApVBMVEUAAADtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTtVWTyjpf3vMH62t34xsr0pqz////75ObuZXL1sbf50NT97e7+9vfxgYvvdH/zmqIAAACYZubzAAAAJnRSTlMAA0KFud/wQxaH5wZ89X0mzyc/7/YH0HsXhgToRYm44PH3KNHpiMoO7BAAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAABSElEQVQ4y5WU2XaCMBCGBxAUBYoKluJStHZYpKiA7/9qDUsPEzbtf0HODN9JZkkGoJEgShNZQUWeSFMB+jRT59hors46xELTkZduLHjkzcSuzCVFVmvs03rVIKKO/bLEP2Sp4JCU+jjbxGFt7JJ5xzFpBeLQYPwgDKMLF5LDmA/iiOJSPwnxqQAuqe41rnUjzNYFkZj3OE6zJGRQTrw7kBojYz8DxAdbroTZw6ExLuwnizdhS0SYA3zy4fhsZUtIGBm8p4wH+JTBl/Yh8fhVQn4rL5nmlVQbFNs9uLxIffBWlDhgn5SGs+fqnKd1L7iu7sDdEjMr+hDf6UlFv0ClDkx8P+ccRd/BsXBM5f0BbZTRqvu8GUHq+/zKu2Dv6ziAHMV/vVOA01dfLCd+KNhGuwSWYXemi6OeCXH+dnqnlDBlc8xDrz3HfgFT7ZAPkjtZvwAAAABJRU5ErkJggg==');
        }
/*

        var template = window.location.href.toString().search('Template') != -1;
        var sprint = window.location.href.toString().search('Sprint') != -1;
        var preprod = window.location.href.toString().search('Preprod') != -1;
        if(template) {
            if(sprint) {
                $(content).attr('src', '/../Sprint/Public/Files/KPL/img/act' + sign + '.png');
            } else if(preprod) {
                $(content).attr('src', '/../Preprod/Public/Files/KPL/img/act' + sign + '.png');
            }
            else {
                $(content).attr('src', '/../Public/Files/KPL/img/act' + sign + '.png');
            }
        } else {
            if(sprint) {
                $(content).attr('src', '/./Sprint/Public/Files/KPL/img/act' + sign + '.png');
            } else if(preprod) {
                $(content).attr('src', '/./Preprod/Public/Files/KPL/img/act' + sign + '.png');
            }
            else {
                $(content).attr('src', 'Public/Files/KPL/img/act' + sign + '.png');
            }
        }
*/

        td.appendChild(content);
    }
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indtext');
    content = document.createElement('div');
    $(content).attr('class', 'shorttitle');
	if (tooltip) {
		$(content).attr('data-tooltip', description);
		$(content).addClass('tooltip');
	}
    $(content).html(counter + '. ' + shorttitle);
    td.appendChild(content);
    content = document.createElement('div');
    $(content).attr('class', 'longtitle');
    $(content).html(longtitle);
    td.appendChild(content);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indpat');

    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if ((sumnum1 || sumnum1 === 0) && den1 && den1 > 0) {
        $(content).html(sumnum1 + ' av ' + den1);
    } else {
        $(content).html('Ej redovisad');
    }
    td.appendChild(content);
    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if ((num2 || num2 === 0) && den2 && den2 > 0) {
        $(content).html(num2 + ' av ' + den2);
    } else {
        $(content).html('Ej redovisad');
    }
    td.appendChild(content);

    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if ((num3 || num3 === 0) && den3 && den3 > 0) {
        $(content).html(num3 + ' av ' + den3);
    } else {
        $(content).html('Ej redovisad');
    }
    td.appendChild(content);
    content = document.createElement('div');
    $(content).html('&nbsp;');
    td.appendChild(content);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indpct');
    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if (sumpct1 || sumpct1 === 0) {
        $(content).html(sumpct1 + ' %');
    } else {
        $(content).html('- %');
    }
    td.appendChild(content);
    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if (sumpct2 || sumpct2 === 0) {
        $(content).html(sumpct2 + ' %');
    } else {
        $(content).html('- %');
    }
    td.appendChild(content);
    content = document.createElement('div');
    $(content).attr('style', 'padding:5px 0px;');
    if (sumpct3 || sumpct3 === 0) {
        $(content).html(sumpct3 + ' %');
    } else {
        $(content).html('- %');
    }
    td.appendChild(content);
    content = document.createElement('div');
    $(content).html('&nbsp;');
    td.appendChild(content);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'fig');
    content = document.createElement('div');
    $(content).attr('id', 'fig1ind' + counter);
    $(content).attr('style', 'width:140px;height:80px;');
    td.appendChild(content);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'fig');
    content = document.createElement('div');
    $(content).attr('id', 'fig2ind' + counter);
    $(content).attr('style', 'width:140px;height:80px;');
    td.appendChild(content);
    tr.appendChild(td);

    //td = document.createElement('td');
    //$(td).attr('class','indplac');
    //	if (placnum != null && placden != null) {
    //		content = document.createElement('span');
    //		$(content).attr('class','plac1');
    //		$(content).html(placnum);
    //		td.appendChild(content);
    //		content = document.createElement('span');
    //		$(content).attr('class','plac2');
    //		$(content).html('/' + placden);
    //		td.appendChild(content);
    //	}
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    table.appendChild(tr);

    createChart1('fig1ind' + counter, pct1, pct2, pct3, l1, l2, stackednames);
    createChart2('fig2ind' + counter, history);

    var descrGroup = document.getElementById('descrGroup' + group);
    content = document.createElement('div');
    $(content).attr('class', 'indexDescription');
    $(content).html(counter + '. ' + description);
    descrGroup.appendChild(content);

    return returnpoint;

}

function AddSumRow(sumToPrint, txtToPrint) {

    var table = document.getElementById('bodyTable'),
        tr = document.createElement('tr'),
        td,
        content
    ;

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumnum');
    $(td).attr('id', 'indsumnum');
    $(td).html(sumToPrint);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumtxt');
    $(td).html(txtToPrint);
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumfill');
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumfill');
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumfill');
    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'indsumfill');
    tr.appendChild(td);

    //td = document.createElement('td');
    //$(td).attr('class','indsumfill');
    //tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    table.appendChild(tr);

}

function CreateDescrTable(descrGroups) {

    var table = document.getElementById('descrTable'),
        tr = document.createElement('tr'),
        td,
        content
    ;

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    td = document.createElement('td');

    content = document.createElement('div');
    $(content).attr('id', 'indexDescriptionTitle');
    $(content).attr('class', 'indexDescriptionTitle');
    $(content).html('Definition av indikatorer');
    td.appendChild(content);

    for (i = 0; i < descrGroups.length; i++) {
        if (descrGroups[i] != '') {
            content = document.createElement('div');
            $(content).attr('id','indexDescriptionSubTitle' + (i+1));
            $(content).attr('class','indexDescriptionSubTitle');
            $(content).html(descrGroups[i]);
            td.appendChild(content);
        }

        content = document.createElement('div');
        $(content).attr('id', 'descrGroup' + (i + 1));
        $(content).attr('class', 'indexDescription');
        td.appendChild(content);
    }

    tr.appendChild(td);

    td = document.createElement('td');
    $(td).attr('class', 'margin');
    tr.appendChild(td);

    table.appendChild(tr);
}

$(function () {

    //$('a.infolink').hover(function () {
    //	$('> div', this).fadeIn();
    //},
    //function () {
    //	$('> div', this).fadeOut();
    //});

    $(document).on('click', 'a.printLink', function () {
        //window.print();
        document.execCommand('print', false, null);
    });

});
